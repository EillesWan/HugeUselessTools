from tkinter import *
from tkinter.messagebox import *
import random

root = Tk()
root.title("世界上最nb，运算最快的计算器")
root.geometry("500x300")

la1 = Label(root, text="输入表达式，怎样都可以")
la1.pack()

entry_var = StringVar()
e1 = Entry(root, width=20, textvariable=entry_var)
e1.pack()
entry_var.set('输入表达式')

answerVar = StringVar()
e2 = Entry(root, width=20, textvariable=answerVar)
e2.pack()


def change_state():
    var = e1.get()  # 调用get()方法，将Entry中的内容获取出来
    print(var)
    answerVar.set(str(
        (
                random.randint(1, 10000000000) + random.random() * random.random()
        ) ** random.random()
    ))
    if askyesno("结果正确吗？", "结果正确吗？"):
        showinfo("谢谢支持！", "谢谢支持！")
    else:
        showwarning("你就说快不快吧！", "你就说快不快吧！")


button = Button(root, text='计算', command=change_state)
button.pack()

if __name__ == '__main__':
    mainloop()
